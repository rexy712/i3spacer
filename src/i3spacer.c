/**
	This file is a part of the i3spacer project
	Copyright (C) 2019 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "config.h"
#include <gtk/gtk.h>

static void screen_changed(GtkWidget *widget, GdkScreen *old_screen, gpointer userdata){
    GdkScreen *screen = gtk_widget_get_screen(widget);
    GdkVisual *colormap = gdk_screen_get_rgba_visual(screen);

    if (!colormap)
        colormap = gdk_screen_get_system_visual(screen);

    gtk_widget_set_visual(widget, colormap);
}
static gboolean keypress_callback(GtkWidget* widget, GdkEventKey* event, gpointer userdata){
	switch(event->keyval)
	{
	case GDK_KEY_q:
	case GDK_KEY_Q:
		gtk_main_quit();
		break;
	};
	return FALSE;
}

int main(int argc, char** argv){
    gtk_init(&argc, &argv);

    GtkWidget *window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "i3spacer");
    g_signal_connect(G_OBJECT(window), "delete-event", gtk_main_quit, NULL);

    gtk_widget_set_app_paintable(window, TRUE);

    g_signal_connect(G_OBJECT(window), "screen-changed", G_CALLBACK(screen_changed), NULL);
		g_signal_connect(G_OBJECT(window), "key_press_event", G_CALLBACK(keypress_callback), NULL);

		#ifdef I3SPACER_DECORATE_WINDOW
    	gtk_window_set_decorated(GTK_WINDOW(window), TRUE);
		#else
			gtk_window_set_decorated(GTK_WINDOW(window), FALSE);
		#endif

    screen_changed(window, NULL, NULL);

    gtk_widget_show_all(window);
    gtk_main();

    return 0;
}

